<?xml version="1.0" encoding="UTF-8"?>
<rss version="2.0" xmlns:media="https://search.yahoo.com/mrss/" xmlns:atom="https://www.w3.org/2005/Atom">
  <channel>
    <title><![CDATA[Beyond Business]]></title>
    <link>https://www.business-standard.com/beyond-business</link>
    <description><![CDATA[Interesting features and columns to track the world of books, art, music, sports, automobiles, spending and leisure]]></description>
    <copyright>Copyright business-standard.com</copyright>
    <lastBuildDate>Wed, 22 Jan 2020 02:34:12 +0530</lastBuildDate>
    <language>en-us</language>
    <image>
      <title>Beyond Business</title>
      <url>https://www.business-standard.com/beyond-business</url>
      <link>https://www.business-standard.com/beyond-business</link>
    </image>
    <atom:link href="https://www.business-standard.com/rss/beyond-business-104.rss" rel="self" type="application/rss+xml"/>
    <item>
      <title><![CDATA[Trump, 'stable genius']]></title>
      <link>https://www.business-standard.com/article/beyond-business/trump-stable-genius-120011900935_1.html</link>
      <guid>https://www.business-standard.com/article/beyond-business/trump-stable-genius-120011900935_1.html</guid>
      <description><![CDATA[It reads like a horror story, an almost comic immorality tale]]></description>
      <pubDate>Sun, 19 Jan 2020 23:41:00 +0530</pubDate>
    </item>
    <item>
      <title><![CDATA[Sunil Kant Munjal recounts his father and uncles' journey of building Hero]]></title>
      <link>https://www.business-standard.com/article/beyond-business/sunil-kant-munjal-recounts-his-father-and-uncles-journey-of-building-hero-120011800893_1.html</link>
      <guid>https://www.business-standard.com/article/beyond-business/sunil-kant-munjal-recounts-his-father-and-uncles-journey-of-building-hero-120011800893_1.html</guid>
      <description><![CDATA[The Munjal brothers knew bicycles. They did not have any capital, but possessed the technical knowledge and skills to make their mark in the rapidly growing bicycle industry, he writes]]></description>
      <pubDate>Sat, 18 Jan 2020 20:51:00 +0530</pubDate>
    </item>
    <item>
      <title><![CDATA[Frames per Second: Remember Derozio, enfant terrible of Bengali Renaissance]]></title>
      <link>https://www.business-standard.com/article/beyond-business/frames-per-second-remember-derozio-enfant-terrible-of-bengali-renaissance-120011701120_1.html</link>
      <guid>https://www.business-standard.com/article/beyond-business/frames-per-second-remember-derozio-enfant-terrible-of-bengali-renaissance-120011701120_1.html</guid>
      <description><![CDATA[An almost forgotten Bengali film casts light on the current turmoil on campuses]]></description>
      <pubDate>Fri, 17 Jan 2020 17:57:00 +0530</pubDate>
    </item>
    <item>
      <title><![CDATA[Sony WI-1000XM2 review: Hi-res audio, noise cancellation make this a winner]]></title>
      <link>https://www.business-standard.com/article/technology/sony-wi-1000xm2-review-hi-res-audio-noise-cancellation-make-this-a-winner-120011701067_1.html</link>
      <guid>https://www.business-standard.com/article/technology/sony-wi-1000xm2-review-hi-res-audio-noise-cancellation-make-this-a-winner-120011701067_1.html</guid>
      <description><![CDATA[On first look, the Sony WI-1000XM2 appears to be an all-rounder in its segment. But is it? Let's find out]]></description>
      <media:content url="https://bsmedia.business-standard.com/_media/bs/img/article/default/1200117/thumb-120011701067.jpg"/>
      <pubDate>Fri, 17 Jan 2020 17:31:00 +0530</pubDate>
    </item>
    <item>
      <title><![CDATA[Dell Inspiron 13 7391 review: Potent convertible notebook that oozes luxury]]></title>
      <link>https://www.business-standard.com/article/technology/dell-inspiron-13-7391-review-potent-convertible-notebook-that-oozes-luxury-120011700388_1.html</link>
      <guid>https://www.business-standard.com/article/technology/dell-inspiron-13-7391-review-potent-convertible-notebook-that-oozes-luxury-120011700388_1.html</guid>
      <description><![CDATA[The Dell Inspiron 13 7391 notebook's premium aluminium construction, portable form factor, good on-battery time and sleek performance make it a complete package in its segment]]></description>
      <media:content url="https://bsmedia.business-standard.com/_media/bs/img/article/default/1200117/thumb-120011700388.jpg"/>
      <pubDate>Fri, 17 Jan 2020 11:42:00 +0530</pubDate>
    </item>
    <item>
      <title><![CDATA[Ascent of a techno-sceptic]]></title>
      <link>https://www.business-standard.com/article/beyond-business/ascent-of-a-techno-sceptic-120011200982_1.html</link>
      <guid>https://www.business-standard.com/article/beyond-business/ascent-of-a-techno-sceptic-120011200982_1.html</guid>
      <description><![CDATA[Is it weird that a CEO can be considered up-and-coming? Yes, but of course everything about the new nearly normal is weird]]></description>
      <media:content url="https://bsmedia.business-standard.com/_media/bs/img/article/default/1200112/thumb-120011200982.jpg"/>
      <pubDate>Sun, 12 Jan 2020 23:41:00 +0530</pubDate>
    </item>
    <item>
      <title><![CDATA[In the Premier League, VAR has exposed the limits of the laws]]></title>
      <link>https://www.business-standard.com/article/sports/in-the-premier-league-var-has-exposed-the-limits-of-the-laws-120011001660_1.html</link>
      <guid>https://www.business-standard.com/article/sports/in-the-premier-league-var-has-exposed-the-limits-of-the-laws-120011001660_1.html</guid>
      <description><![CDATA[VAR has been the talk of the Premier League with many games so far being decided by the smallest of margins]]></description>
      <media:content url="https://bsmedia.business-standard.com/_media/bs/img/article/default/1200110/thumb-120011001660.jpg"/>
      <pubDate>Fri, 10 Jan 2020 22:25:00 +0530</pubDate>
    </item>
    <item>
      <title><![CDATA[How Rupert Murdoch is influencing Australia's bushfire debate]]></title>
      <link>https://www.business-standard.com/article/current-affairs/how-rupert-murdoch-is-influencing-australia-s-bushfire-debate-120011001602_1.html</link>
      <guid>https://www.business-standard.com/article/current-affairs/how-rupert-murdoch-is-influencing-australia-s-bushfire-debate-120011001602_1.html</guid>
      <description><![CDATA[Critics see a concerted effort to shift blame, protect conservative leaders and divert attention from climate change]]></description>
      <pubDate>Fri, 10 Jan 2020 21:49:00 +0530</pubDate>
    </item>
  </channel>
</rss>